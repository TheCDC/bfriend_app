.. bfriend_app documentation master file, created by
   sphinx-quickstart on Mon Apr 10 00:44:50 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to bfriend_app's documentation!
=======================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   app
   dbrepl
   flask_app
   generate_users
   run
   setup


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
