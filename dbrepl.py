import app.db as db
import sqlite3


def main():
    while True:
        try:
            query = input(">>>")
        except (EOFError, KeyboardInterrupt):
            # gracefully quit
            print()
            quit()
        try:
            # just run that query!
            with db.connect() as (_, c):
                c.execute(query)
                print('\n'.join(map(str, c.fetchall())))

        except sqlite3.OperationalError as e:
            # alert user to errors
            print(e)


if __name__ == '__main__':
    main()
