import contextlib
import sqlite3
import app.config as config
import logging


@contextlib.contextmanager
def connect():
    """A simple context manager to have a short way
    of connecting to the DB and automatically commiting changes
    and closing the connection."""
    connection = sqlite3.connect(config.DB_PATH)
    c = connection.cursor()
    # force-enable foreign key logic
    # this allows cascading directives to happen
    c.execute('pragma foreign_keys = ON;')
    yield connection, c
    connection.commit()
    connection.close()


logging.info("DB lives as %s in %s", __name__, __file__)
