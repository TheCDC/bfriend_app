#!/usr/bin/python3

# https://flask-login.readthedocs.io/en/latest/
# cd ~/Desktop/bfriend_app; python3 flask_app.py

# the main event
import flask
# magic for handling user sessions
import flask_login
# Use bootstrap for lots of reasons
import flask_bootstrap
# defaults for initialiazing the db and etc.
import app.initialize_resources as initialize_resources
# default app-wide globals
import app.config as config
# abstraction layer for handling user info
import app.users as users
# questions layer
import app.questions as questions
# challenges module
import app.challenges as challenges
# fun stuff
import app.api as api
# for files and things
import app.cdn as cdn
import app.cdn.imgutils
# in-house api for list of schools
import app.schools as schools
# manage user geodata
import app.locations as locations
# because logging
import logging
import logging.handlers
# from flask import request
# for file and path manipulation
import os
import sys
import math
# the poop API
import app.poop
import app.orm as orm
import peewee as pw

initialize_resources.main()

flask_app = flask.Flask(__name__)

# db setup routines
initialize_resources.main()
# ========== Logging ==========
formatter = logging.Formatter(
    "[%(asctime)s] {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s")
# logger = logging.getLogger('flask')
logger = flask_app.logger
logger.setLevel(logging.DEBUG)
logger.addHandler(logging.StreamHandler())

# setting the secret is necessary to use the session
flask_app.secret_key = "bfriend.lunar2017yoloswag"
# logging.warning(
#     "WARNING secret key not actually secret! flask_application is insecure!")

# site-wide globals
flask_app.config["UPLOAD_FOLDER"] = config.UPLOAD_FOLDER
flask_app.config["DB_PATH"] = config.DB_PATH


# instantiate and initialize login manager
login_manager = flask_login.LoginManager()
login_manager.init_app(flask_app)
login_manager.login_view = "/"
# initialize bootstrap
flask_bootstrap.Bootstrap(flask_app)


# special recipe for managing logins and user sessions
# must be a function which returns our user object given a uniue id
@login_manager.user_loader
def load_user(unicode_id):
    """Deserialize the current user."""
    try:
        # assume a user is logged in
        u = users.User.from_unicode(unicode_id)
        u.verify_integrity()
        return u
    except users.AuthenticationError:
        # handle the user either being not logged in
        # or else otherwise invalid
        return None


@flask_app.before_request
def _db_connect():
    """Ensure that the db connection is available for every request."""
    orm.db.connect()

# This hook ensures that the connection is closed when we've finished
# processing the request.


@flask_app.teardown_request
def _db_close(exc):
    if not orm.db.is_closed():
        orm.db.close()


@flask_app.after_request
def redirect_to_signin(response):
    """Gracefully redirect when not logged in"""
    if response.status_code == 401:
        return flask.redirect(flask.url_for("home", next=flask.request.url))
    else:
        return response


@flask_app.route("/test")
def test():
    return flask.redirect(flask.url_for("other_profile", id=1))


@flask_app.route("/about")
def about():
    """About Us page."""
    return flask.render_template("about.html")


@flask_app.route("/bootstrap")
def bootstrap_example():
    """A quick test render of bootstrap"""
    return flask.render_template("bootstrap_test.html")


@flask_app.route("/<filename>")
def test_poop(filename):
    """The poop and haha APIs"""
    if app.poop.match_endpoint(filename):
        return app.poop.result(filename)
    elif "ha" in filename.lower():
        return ''.join("😂" for i in range(filename.lower().count("ha")))
    else:
        return flask.redirect(flask.url_for("home"))


@flask_app.route("/")
def root():
    """Landing Page"""
    if flask_login.current_user._get_current_object().is_authenticated:
        return flask.redirect(flask.url_for("home"))
    else:
        return flask.redirect(flask.url_for("landing"))


@flask_app.route("/home")
def home():
    """Generic homepage."""
    return flask.render_template("home.html")


@flask_app.route("/landing")
def landing():
    """first page."""
    return flask.render_template("landing.html")


@flask_app.route("/login", methods=["GET", "POST"])
def login():
    """Page for user login."""
    if flask.request.method == "GET":
        if flask_login.current_user._get_current_object().is_authenticated:
            return flask.redirect(flask.url_for("home"))
        return flask.render_template("login.html")
    else:
        try:
            username = flask.request.form["username"]
            password = flask.request.form["password"]
            # attempt to reconstitute the user from the db
            cur_user = users.User.login(username, password,)
            # magic that sets the session for the logged in user
            flask_login.login_user(cur_user)
            return flask.redirect(flask.url_for("home"))
        except users.AuthenticationError as e:
            # handle inability to login for various reasons
            return flask.render_template("login.html",
                                         message=str(e).capitalize())


@flask_app.route("/register", methods=["GET", "POST"])
def register():
    """Page for new user registration."""
    schools_list = schools.School.all_schools()
    if flask.request.method == "GET":
        if flask_login.current_user._get_current_object().is_authenticated:
            return flask.redirect(flask.url_for("home"))
        return flask.render_template("register.html",
                                     schools=schools_list)
    else:

        try:
            username = flask.request.form["username"]
            password = flask.request.form["password"]
            response = None
            try:
                target_school = flask.request.form["school"]
            except KeyError:
                raise ValueError("Oops, did you pick a school?")
            school = schools.School.by_id(target_school)

            users.User.register(username, password)
            u = users.User.from_username(username)
            u.school = school
            u.save()
            flask_login.login_user(u)
            logging.info("User registration: name='%s' id='%s'",
                         u.name, u.id)
            response = flask.redirect(flask.url_for("home"))
        except (users.AuthenticationError, ValueError) as e:
            # exception when user already exists
            response = flask.render_template("register.html",
                                             message=str(e).capitalize(),
                                             schools=schools_list)

    return response


@flask_app.route("/logout")
@flask_login.login_required
def logout():
    """Page that logs out the current user."""
    flask_login.logout_user()
    return flask.redirect(flask.url_for("login"))


@flask_app.route("/bad")
def bad():
    """Page for generic errors."""
    message = flask.request.args.get("message")
    return flask.render_template("bad.html", message=message)


@flask_app.route("/points")
@flask_login.login_required
def points():
    # Get current user
    u = flask_login.current_user._get_current_object()
    # Get users points
    points = challenges.Objective.users_points(u)
    return str(points)


@flask_app.route("/profile")
@flask_login.login_required
def profile():
    """Page to view the current user's profile."""
    logging.info("Visited profile")
    if not flask_login.current_user._get_current_object().is_authenticated:
        flask_login.logout_user()
        return flask.redirect(flask.url_for("home"))

    return flask.render_template("users_connections.html")


@flask_app.route("/profile/edit", methods=["GET", "POST"])
def profile_edit():
    """Page to edit the crrent user's profile."""
    cur_user = flask_login.current_user
    if not cur_user.is_authenticated:
        return flask.redirect(flask.url_for("home"))
    m = flask.request.method
    if m == "GET":
        return flask.render_template("profile_edit.html",
                                     schools=schools.School.all_schools())
    elif m == "POST":
        # case that a user is trying to make edits
        new_bio = flask.request.form["bio"]
        try:
            new_school = schools.School.by_id(flask.request.form["school"])
            cur_user.school = new_school
        except KeyError:
            pass
        except pw.DoesNotExist:
            pass
        cur_user.bio = new_bio
        cur_user.save()
        # check if there is a file uploaded
        logging.debug("All files: %s", (flask.request.files,))
        if "photo" in flask.request.files:
            new_photo = flask.request.files["photo"]
            logging.debug("Possible photo upload: %s", (new_photo,))
            filename = new_photo.filename

            # handle empty filename
            if filename != '':
                # save a reference to the old photo
                old = cur_user.photo
                p = cdn.File.from_memory(new_photo)
                # force the image to corect orientation
                cdn.imgutils.rotate_to_exif(p)
                logging.debug("New profile photo: %s", (p,))
                # set the current user's profile photo to the new one
                cur_user.photo = p
                print("New assigned photo:", p)
                try:
                    # peewee save ORM object
                    cur_user.save()
                    old.erase()
                except AttributeError:
                    # it's ok if the old photo is invalid
                    pass
        cur_user.save()
        return flask.redirect(cur_user.url)


@flask_app.route("/static/<path:filename>")
def serve_static(filename):
    """Serve a static resource."""
    return flask.send_from_directory("static", filename)


@flask_app.route("/img/<filename>")
def img(filename):
    """Serve an arbitrary image."""
    return flask.send_from_directory(
        os.path.join(flask_app.config["UPLOAD_FOLDER"]), filename
    )


@flask_app.route("/user/<path:path>")
def view_profile(path):
    """Return a user's profile or photo."""
    route = path.split("/")
    target_username = route[0]
    # assert route[1] == "face"
    if len(route) > 1:
        if route[1] == "face":
            # try:
            try:
                p = users.User.from_username(target_username).photo.path
            except AttributeError:
                p = os.path.join(config.DATA_DIR, config.DEFAULT_PROFILE_PHOTO)
            return flask.send_from_directory(os.path.dirname(p),
                                             os.path.basename(p))
    else:
        try:
            # Get the target user's points
            target = users.User.from_username(target_username)
            user_score = str(challenges.Objective.users_points(target))
            return flask.render_template(
                "profile.html",
                other_user=target,
                points=user_score,
                recent_connections=challenges.recent_connections(target))
        except app.users.AuthenticationError:
            flask.abort(404)

    # return users.User.from_username(username).photo.serve()


@flask_app.route("/user")
def other_profile():
    """View any user, self or otherwise."""
    target = flask.request.args.get("id")
    try:
        return flask.render_template("profile.html",
                                     other_user=users.User.from_id(target))
    except users.AuthenticationError:
        flask.abort(404)


@flask_app.route("/search")
def search_users():
    """Page for searching for other users."""
    target = flask.request.args.get("username")
    try:
        page_num = int(flask.request.args.get("page"))
    except TypeError:
        page_num = 1
    # paginated search query results
    results = users.search_username(target, page=page_num)
    # the next page, if any
    forward_results = users.search_username(target, page=page_num + 1)
    # how many in total
    num_results = users.num_matching(target)
    # if invalid, serach for emptystring
    if target is None:
        target = ''
    more = len(forward_results) > 0 and len(results) > config.USERS_PER_PAGE
    return flask.render_template("search_form.html",
                                 results=results,
                                 query=target,
                                 page=page_num,
                                 num_pages=math.ceil(
                                     num_results / config.USERS_PER_PAGE),
                                 more=more)


@flask_app.route("/leaderboard")
@flask_login.login_required
def leaderboard():
    """Page to view the current user's achievements."""

    u = flask_login.current_user._get_current_object()
    local_leaders = challenges.local_leaders(u)
    global_leaders = challenges.global_leaders()
    return flask.render_template("achievements.html",
                                 local_leaderboard=local_leaders,
                                 global_leaderboard=global_leaders)


@flask_app.route("/users_connections")
@flask_login.login_required
def near_me():
    """Page for listing a users's active handshakes."""
    u = flask_login.current_user._get_current_object()
    # check how many active objectives this user has
    handshakes = challenges.Objective.incoming_handshakes(u)
    # raise ValueError(str(objectives[0].__dict__))
    answers = [o.reason for o in handshakes]
    return flask.render_template("users_connections.html",
                                 handshakes=handshakes,
                                 answers=answers)


@flask_app.route("/to_find")
@flask_login.login_required
def to_find():
    """Page for listing a users's objectives."""
    u = flask_login.current_user
    # check how many active objectives this user has
    active_objectives = challenges.Objective.active_objectives(u)
    print("ACTIVE:", active_objectives)
    # create more if they are beneath threshold
    try:
        # try to make objectives until they have the default amount
        if(len(active_objectives) < config.OBJECTIVES_PER_DAY):
            for _ in range(config.OBJECTIVES_PER_DAY - len(active_objectives)):
                challenges.Objective.new_objective(u)
    except challenges.WeirdoFoundException:
        # no matches :'(
        objectives = []

    objectives = challenges.Objective.active_objectives(u)
    answered = [questions.Answer.how_answered(u, o.reason) for o in objectives]
    return flask.render_template("to_find.html",
                                 objectives=objectives,
                                 answers=answered)


@flask_app.route("/delete_profile", methods=["POST"])
@flask_login.login_required
def delete_user():
    """Delete your own profile."""
    u = flask_login.current_user._get_current_object()
    u.erase()
    return flask.redirect(flask.url_for("root"))


@flask_login.login_required
@flask_app.route("/survey", methods=["GET", "POST"])
def survey():
    """Page for survey questions."""
    user = flask_login.current_user
    try:
        page_number = int(flask.request.args.get("page"))
    except TypeError:
        page_number = 0
    answered_questions = questions.Question.browse_answered(
        user=user,
        page=page_number,
        page_size=None
    )
    num_pages = questions.Question.count_answered(
        user) // config.QUESTIONS_PER_PAGE
    remaining = len(questions.Question.browse_answered(
        user=user, page=page_number + 1))
    # logging.debug("Remaining Q's for %s: %s", remaining, user)
    more_exist = remaining > 0
    method = flask.request.method
    message = None
    if method == "GET":
        # case of GETting questions
        try:
            q = questions.Question.unanswered_question(
                flask_login.current_user)
        except questions.Question.DoesNotExist:
            message = "Oops! You answered all the questions."
            q = None
        return flask.render_template("survey.html",
                                     q=q,
                                     answered=answered_questions,
                                     more=more_exist,
                                     message=message,
                                     page=page_number,
                                     num_pages=num_pages
                                     )
    elif method == "POST":
        # case of POSTing answer to question
        u = flask_login.current_user._get_current_object()
        q = questions.Question.from_id(flask.request.form["question"])
        ai = flask.request.form["response"]
        a = questions.Answer(user=u, question=q, response=ai)
        a.save()

        return flask.redirect(flask.url_for("survey"))


@flask_login.login_required
@flask_app.route("/handshake", methods=["POST"])
def handshake():
    # create object from the given id
    cur_user = flask_login.current_user._get_current_object()
    form = flask.request.form
    obj_id = form["target_objective"]
    # return str(flask.request.form)
    objective = challenges.Objective.from_id(obj_id)
    message = None
    success = False
    if form["handshake_action"] == "extend":
        # if location is invalid then just redirect
        try:
            user_lat = float(form["user_location_lat"])
            user_long = float(form["user_location_long"])
            locations.UserLocation.track(cur_user, user_lat, user_long)
        except ValueError:
            return flask.redirect(flask.url_for("to_find"))

        if objective.source != cur_user:
            flask.abort(401)
        # set objective status to 'handshake extended' stage
        if((objective.status == 0 or objective.status == 1)):
            recipient = objective.target
            objective.status = 1
            objective.save()
            # they can only extend a handshake from to_find, so send them back
            return flask.redirect(flask.url_for("to_find"))
            return flask.render_template("sent_confirm.html",
                                         recipient=recipient,
                                         message=message)
    elif form["handshake_action"] == "receive":
        # if location is invalid then just redirect

        try:
            user_lat = float(form["user_location_lat"])
            user_long = float(form["user_location_long"])
            locations.UserLocation.track(cur_user, user_lat, user_long)
        except ValueError:
            return flask.render_template("received_confirmation.html",
                                         message=message,
                                         success=success)

            return flask.render_template("received_confirmation.html",
                                         message=message,
                                         success=success)
        # check to see if user is the target and status is in
        # 'handshake extended'stage
        if(cur_user == objective.target and
           objective.status == 1):
            source = objective.source
            try:
                if locations.close_enough(source, cur_user):
                    objective.status = 2
                    objective.save()
                    message = "Got 'em!"
                    success = True
                else:
                    objective.status = 0
                    objective.save()
                    message = "You can't tag someone you're not close to, namsayin?"
                    success = False
            except pw.DoesNotExist:
                objective.status = 0
                objective.save()
                message = "You can't tag someone you're not close to, namsayin?"
                success = False

            return flask.render_template("received_confirmation.html",
                                         source=source,
                                         message=message,
                                         success=success)
            # return flask.redirect(flask.url_for("users_connections"))
    else:
        flask.abort(401)


@flask_app.route("/api/<path:path>")
def serve_api(path):
    """Delegate all requests to /api/ to the api"""
    try:
        return api.incoming_handshakes(path)
    except ValueError:
        flask.abort(400)


@flask_app.route("/api/universities/<filename>")
def search_universities(filename):
    try:
        return api.search_universities(filename)
    except ValueError:
        flask.abort(400)


def main():
    flask_app.config.update(TEMPLATES_AUTO_RELOAD=True)

    port = 5000
    host = '0.0.0.0'
    print("BFriend app running under {} from {}".format(__name__, __file__))
    print("port={} host={}".format(port, host))
    flask_app.run(debug=True, port=port, host=host)


if __name__ == '__main__':
    main()
