var x = document.getElementById("location_id");
var btns = document.getElementsByClassName("gottem-button");

var hideButtons = function() {
    // Hide all gottem buttons 
    for (var i = btns.length - 1; i >= 0; i--) {
        if (!btns[i].classList.contains("hidden")) {

            btns[i].classList.add("hidden")
        }
    }
}

var showButtons = function() {
    // show all gottem buttons
    for (var i = btns.length - 1; i >= 0; i--) {
        if (btns[i].classList.contains("hidden")) {

            btns[i].classList.remove("hidden")
        }

    }
}

var locationError = function(arg) {
    // case of something goHi9ding buttons")
    translate = {
        1: "PERMISSION_DENIED",
        2: "POSITION_UNAVAILABLE",
        3: "TIMEOUT"
    }
    console.log("locationError: code=" + translate[arg.code]);
    x.innerHTML = "Your coordinates are unavailable at this time :("

    hideButtons();
}

var showPosition = function(position) {
    showButtons();
    lat = position.coords.latitude;
    long = position.coords.longitude;
    console.log("Lat=" + lat + " Long=" + long)
    encodeLocation(lat, long);
    if (lat !== "" && long != "") {

        x.innerHTML = "Latitude: " + position.coords.latitude +
            "<br>Longitude: " + position.coords.longitude;
    } else {}
}



var encodeLocation = function(lat, long) {
    // store location data in all forms in objectives cards on /to_find
    lats = document.getElementsByClassName("user-lat")
    longs = document.getElementsByClassName("user-long")
    for (var i = 0; i < lats.length; i++) {

        lats[i].value = lat;
        longs[i].value = long;
    }

    showButtons();
}

var getLocation = function() {
    console.log("Reading location data")
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition, locationError);
    } else {
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
}

var checkForLocationData = function() {
    getLocation();
    if (x.innerHTML != "Receiving location data...") {
        setTimeout(function() {
            if (location.pathname.includes("receive_handshake")) {
                window.location = "/users_connections";
            } else if (location.pathname.includes("extend_handshake")) {
                window.location = "/to_find";
            }
            //logic
        }, 2000);
    }
}

x.innerHTML = "Retrieving location..."
checkForLocationData();
setInterval(checkForLocationData, 5000);
hideButtons();
