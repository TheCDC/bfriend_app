var incomingRequests = function() {
    var xhr = new XMLHttpRequest();
    var name = document.getElementById("USERNAME").innerHTML;
    xhr.open('GET', "/api/incoming/" + name, true);
    xhr.send();
    xhr.addEventListener("readystatechange", processRequest, false);

    function processRequest(e) {
        if (xhr.readyState == 4 && xhr.status == 200) {
            var response = JSON.parse(xhr.responseText);
            renderResults(response.num_incoming);
        }
    }
}

var renderResults = function(response) {
    if (response > 0) {
        document.getElementById("incoming").innerHTML = response;
    }
}

incomingRequests()
setInterval(incomingRequests, 5000)
