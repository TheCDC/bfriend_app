#!/usr/bin/env python3
import app.orm as orm
import peewee as pw


class School(orm.BaseModel):
    id = pw.PrimaryKeyField()
    name = pw.CharField()

    class Meta:
        db_table = "schools"

    @classmethod
    def by_name(cls, name):
        return School.select().where(
            (School.name == name)
        ).get()

    @classmethod
    def all_schools(cls):
        return list(
            School.select().where(1 == 1)
        )

    @classmethod
    def by_id(cls, target):
        return School.select().where(School.id == target).get()
