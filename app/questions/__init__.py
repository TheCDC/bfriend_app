import app.db as db
import peewee as pw
import app.orm as orm
import typing
import flask_login
import app.config as config
import app.cdn as cdn
import app.users as users
import re
import random
from passlib.hash import pbkdf2_sha256
from collections import namedtuple
import logging


class QuestionUndefinedError(Exception):
    pass


class UnansweredQuestionError(Exception):
    pass


class Question(orm.BaseModel):
    """ORM Model representing an answerable question.
    """
    id = pw.PrimaryKeyField()
    body = pw.CharField(unique=True, max_length=500)
    answers = pw.CharField(max_length=500)

    class Meta():
        db_table = "questions"

    @property
    def answers_list(self):
        """Return list of possible answers for this question."""
        return str(self.answers).split("|")

    @staticmethod
    def random_questions(n=1) -> "Question":
        try:
            return list(Question.select().order_by(pw.fn.rand()).limit(n))
        except pw.OperationalError:
            return list(Question.select().order_by(pw.fn.rand()).limit(n))

    @staticmethod
    def unanswered_question(u):
        sub = Question.select(Question.id).join(
            Answer).where(
            Answer.user == u.id)
        try:
            q = Question.select().where(~(Question.id << sub)
                                        ).order_by(pw.fn.rand()).get()
        except pw.OperationalError:
            q = Question.select().where(~(Question.id << sub)
                                        ).order_by(pw.fn.rand()).get()

        return q

    @classmethod
    def browse_answered(cls,
                        user: users.User,
                        page: int=None,
                        page_size: int =None) -> typing.List["Question"]:
        """Fetch a paginated list of all
        of a target users's answered questions."""
        try:
            assert isinstance(user, users.User)
            assert isinstance(page, int)
            assert isinstance(page_size, int)
        except AssertionError:
            pass
        if page is None:
            page = 0
        if page_size is None:
            page_size = config.QUESTIONS_PER_PAGE
        old_answers = Answer.select().where(
            Answer.user == user.id).order_by(
            Answer.date.desc()).paginate(page, page_size)
        return old_answers

    @classmethod
    def count_answered(cls, u: users.User):
        return len(Answer.select().where(Answer.user == u.id))

    @classmethod
    def from_id(cls, target_id):
        return Question.select().where(Question.id == target_id).get()


class Answer(orm.BaseModel):
    id = pw.PrimaryKeyField()
    date = pw.DateTimeField(default=pw.datetime.datetime.now)
    user = pw.ForeignKeyField(users.User, on_delete="CASCADE", null=False)
    question = pw.ForeignKeyField(Question, on_delete="CASCADE", null=False)
    response = pw.IntegerField()

    class Meta:
        db_table = "answers"

    @staticmethod
    def new_answer(source_user, target_question, answer_index) -> "Answer":
        try:
            ai = int(answer_index)
        except ValueError:
            raise ValueError(
                "Invalid answer_index, must be int or represent int.")
        a = Answer(user=source_user,
                   question=target_question,
                   response=answer_index)
        a.save()

        if ai < 0 or ai > len(target_question.answers):
            raise IndexError("Invalid answer [{}] to question [{}]".format(
                ai, target_question))
        return Answer(source_user, target_question)

    @staticmethod
    def how_answered(source_user: users.User, target_question: Question):
        result = Answer.select().where(
            (Answer.user == source_user.id) &
            (Answer.question == target_question)
        ).get()
        return result

    @property
    def response_body(self) -> str:
        return self.question.answers_list[self.response]


QuestionRelation = namedtuple(
    "QuestionRelation", ["source", "target", "answer"])
