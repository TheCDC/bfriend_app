"""The config module.
Contains constants for paths of various resource folders
and defualt questions and whatnot.
"""
import os
import json
from os import path
import pprint as pp
import types
import logging
OBJECTIVES_PER_DAY = 5
PACKAGE_DIR = path.abspath(path.join(__file__, path.pardir, path.pardir))
logging.debug("Package directory: %s", (PACKAGE_DIR,))
USER_HOME = path.expanduser("~")
APPDATA_DIR = path.join(USER_HOME)
DATA_DIR = path.join(PACKAGE_DIR, "appdata")
PATHS = [
    DATA_DIR,
    path.join(DATA_DIR, "uploads"),
    path.join(DATA_DIR, "log")
]
RESOURCES_DIR = os.path.join(PACKAGE_DIR, "app", "resources")
UPLOAD_FOLDER = path.join(DATA_DIR, "uploads")
DB_PATH = path.join(DATA_DIR, "database.db")
DEFAULT_PROFILE_PHOTO = path.join(
    PACKAGE_DIR, "app", "static", "img", "default_profile.png")
USERS_PER_PAGE = 10
QUESTIONS_PER_PAGE = 10
CHALLENGE_POINTS = 100

DEFAULT_QUESTIONS = json.load(open(path.join(RESOURCES_DIR, "questions.json")))
# meters
DISTANCE_MAX = 50
# a table for all users' active objectives
# Managing the value for status will be interesting
# Curent usage of status:
# 0: instantiated and incomplete
# 1: source user has initiated handshake
# 2: target user has acknowledged handshake
# Future status codes will be filled as needed.


def main():
    pp.pprint([i for i in locals().items() if not i[0].startswith(
        "__") and not isinstance(i[1], types.ModuleType)])


if __name__ == '__main__':
    main()
