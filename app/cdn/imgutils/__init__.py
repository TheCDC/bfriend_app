from PIL import Image, ExifTags
from app import cdn


def rotate_to_exif(target_file: cdn.File):

    try:
        # open the physical file
        image = Image.open(target_file.path)
        for orientation in ExifTags.TAGS.keys():
            if ExifTags.TAGS[orientation] == 'Orientation':
                break
        exif = dict(image._getexif().items())

        if exif[orientation] == 3:
            image = image.rotate(180, expand=True)
        elif exif[orientation] == 6:
            image = image.rotate(270, expand=True)
        elif exif[orientation] == 8:
            image = image.rotate(90, expand=True)
        image.save(target_file.path)
        image.close()

    except (AttributeError, KeyError, IndexError):
        # cases: image don't have getexif
        pass
