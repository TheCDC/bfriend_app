import uuid
import app.config
import os
import app.db as db
import logging
import flask
import shutil
import requests
from werkzeug.utils import secure_filename
import app.orm as orm
import peewee as pw


class File(orm.BaseModel):
    id = pw.PrimaryKeyField()
    date = pw.DateTimeField(default=pw.datetime.datetime.now)
    name = pw.TextField(default="")
    path = pw.TextField(null=True)
    description = pw.TextField(default="")

    class Meta:
        db_table = "files"

    @classmethod
    def from_memory(cls, memfile, owner=None, dest_folder=None,
                    description=None) -> "File":
        """Allow for creation of File from file received in POST request.

        :param memfile: file object in memory to be inserted into the cdn
        :param owner: user to be registered as owning this file
        :param dest_folder: optional target folder in which to store the file
        :param decription: optional text description associated with the file

        """
        # handle default values
        if dest_folder is None:
            dest_folder = app.config.UPLOAD_FOLDER
        if description is None:
            description = ""
        # get a unique physical name
        unique = str(uuid.uuid4())
        og_name = secure_filename(memfile.filename)
        print("OG NAME:", og_name)
        # try to extract the extension
        try:
            ext = '.'.join(og_name.split(".")[1:])
        except IndexError:
            ext = og_name
        # decide on a physical path
        outname = unique + "." + ext
        full_path = os.path.join(
            dest_folder, outname)
        # save it
        memfile.save(full_path)
        # record in db
        new_file, created = File.get_or_create(
            name=og_name, path=full_path, description=description)
        if not created:
            new_file.save()
        # log it
        logging.info("File upload: %s", (og_name, full_path, description))
        return new_file

    @classmethod
    def from_id(cls, target: str) -> "File":
        """Instantiate a file from it's database id.

        :param target_id: the id of the target file in the db

        """
        return File(id=target)

    def serve(self):
        """Directly serve this file through flask."""
        return flask.send_from_directory(
            os.path.dirname(self.path),
            os.path.basename(self.path)
        )

    def erase(self) -> None:
        """Delete this file from disk and DB."""
        try:
            os.remove(self.path)
        except FileNotFoundError:
            pass
        self.delete_instance()

    @property
    def path_only(self) -> str:
        return os.path.dirname(self.path)
