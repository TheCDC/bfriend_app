import app.orm as orm
import app.db as db
import app.schools as schools
import typing
import flask_login
import app.config as config
import app.cdn as cdn
import re
import random
from passlib.hash import pbkdf2_sha256
from collections import namedtuple
import peewee as pw
import logging
safe_regex = re.compile(r"^[a-zA-Z0-9_.-]+$")

QuestionRelation = namedtuple("QuestionRelation", ["a", "b", "answer"])


class AuthenticationError(Exception):
    pass


class User(flask_login.UserMixin, orm.BaseModel):
    """A model representing a user and basuc associated information.
    """
    id = pw.PrimaryKeyField()
    date = pw.DateTimeField(default=pw.datetime.datetime.now)
    name = pw.CharField(unique=True)
    password = pw.CharField(max_length=256)
    bio = pw.TextField(default="")
    photo = pw.ForeignKeyField(cdn.File, null=True, default=None)
    school = pw.ForeignKeyField(schools.School, null=True)

    class Meta():
        db_table = "users"

    @staticmethod
    def register(username: str, password: str) -> "User":
        """Create a new user in db by username and password."""
        # disallow weird usernames
        if not username_is_safe(username):
            raise AuthenticationError(
                "Username must contain only [a-zA-Z0-9_.-]!")
        results = list(User.select().where(User.name == username))
        # there should only be 1 or 0 but just check for any > 0s
        if len(results) > 0:
            raise AuthenticationError("username already exists")
        else:
            # when no conflicts with existing users, create new db record
            hashed_password = hash_password(password)
            new_user = User(name=username,
                            password=hashed_password)
            new_user.save()
            return new_user

    @classmethod
    def login(cls, username: str, password: str):
        try:
            # attempt to deserialize user
            u = User.select().where(
                User.name == username).get()
            hashed = u.password
            # use the hashing lib to verify pw
            if pbkdf2_sha256.verify(password, hashed):
                # record each login
                event = LoginEvent(user=u)
                event.save()
                return u
            else:
                raise Exception("Bad login")
        # ambiguous exception used because peewwe raises weird errors
        except Exception:
            raise AuthenticationError("Invalid credentials!")

    @classmethod
    def from_unicode(cls, unicode_id: str) -> "User":
        try:
            i, username = unicode_id.split("|")
        except ValueError:
            raise AuthenticationError("Login no longer valid!")
        u = User.from_id(i)
        if u.name == username:
            return u
        else:
            raise AuthenticationError()

    @classmethod
    def from_id(cls, target_id: int) -> "User":
        """Construct a user from just their id."""
        # refer to a user only by record id
        try:
            return User.select().where(User.id == target_id).get()
        except Exception:
            raise AuthenticationError()

    @classmethod
    def from_username(cls, username: str) -> "User":
        """Construct a User from just their username."""
        try:
            u = User.select().where(User.name == username).get()
            assert isinstance(u, User)
            return u
        except TypeError:
            raise AuthenticationError("User no longer valid!")

        # The following attributes are already handled by inheritance
        # self.is_active = True
        # self.is_authenticated = True
        # self.is_anonymous = False
    @property
    def is_authenticated(self) -> bool:
        return True

    @property
    def is_active(self):
        return True

    @property
    def is_anonymous(self):
        return False

    @property
    def photo_url(self) -> str:
        """Return the link to view the user's photo."""
        if self.photo is None:
            return "/user/{}/face".format(self.name)
        return "/user/{}/face?{}".format(self.name, self.photo.name)

    @property
    def url(self) -> str:
        """Return the url for the user's profile page."""
        return "/user/{}".format(self.name)

    def get_id(self) -> str:
        """Return the user's unicode id."""
        return "|".join(map(str, [self.id, self.name]))

    def erase(self) -> None:
        """Delete reference to this user in the db."""
        p = self.photo
        self.photo = None
        self.save()
        try:
            p.erase()
        except AttributeError:
            # it's ok if the old image was NULL
            pass
        self.delete_instance()

    def upload_file(self, memfile) -> cdn.File:
        """Stores a file from memory in the cdn,
        the file would usually come from an html form or something."""
        upload = cdn.File.from_memory(memfile)
        upload.owner = self
        return upload

    def verify_integrity(self):
        """Check if a user's attributes are kosher."""
        try:
            self.photo
        except:
            self.photo = None
            self.save()


class LoginEvent(orm.BaseModel):
    """A model to record instances of user logins."""
    id = pw.PrimaryKeyField()
    user = pw.ForeignKeyField(
        User, null=True, default=None, on_delete="CASCADE")
    date = pw.DateTimeField(default=pw.datetime.datetime.now)


def username_is_safe(s: str) -> bool:
    """Return whether the username is allowed."""
    return safe_regex.match(s)


def num_matching(name: str) -> int:
    """Return number of users whose names contain target."""
    search = User.select().where(
        User.name.contains(name)
    )
    return search.count()


def search_username(target_username: str,
                    page: int = None,
                    page_size: int = None) -> typing.List[User]:
    """Return a list of users partially matching the target_username."""
    # let an empty search return no results
    if target_username is None:
        return []
    elif len(target_username) == 0:
        return []
    if page_size is None:
        page_size = config.USERS_PER_PAGE

    search = User.select().where(
        User.name.contains(target_username)
    )
    if page is None:
        return list(search.limit(100))
    else:
        return list(search.paginate(page, page_size))


def hash_password(p: str):
    return pbkdf2_sha256.hash(p)
