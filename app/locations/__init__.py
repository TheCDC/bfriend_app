import app.orm as orm
import peewee as pw
import app.users as users
import app.config as config
from geopy.distance import vincenty


class UserLocation(orm.BaseModel):
    """Associate a user and location data."""
    id = pw.PrimaryKeyField()
    user = pw.ForeignKeyField(users.User)
    lat = pw.DecimalField(max_digits=10, decimal_places=7)
    long = pw.DecimalField()

    @classmethod
    def user_location(cls, target: users.User):
        """Return a user's UserLocation."""
        return UserLocation.select().where(UserLocation.user == target).get()

    @classmethod
    def track(cls, target: users.User, user_lat: float, user_long: float):
        """Set a user's latitude and longitude."""
        try:
            loc = UserLocation.select().where(
                UserLocation.user == target).get()
            loc.lat = user_lat
            loc.long = user_long
            loc.save()
        except pw.DoesNotExist:
            loc = UserLocation(user=target,
                               lat=user_lat,
                               long=user_long)
            loc.save()


def close_enough(user1: users.User, user2: users.User):
    l1 = UserLocation.user_location(user1)
    l2 = UserLocation.user_location(user2)

    d = vincenty((l1.lat, l1.long), (l2.lat, l2.long)).meters
    return d < config.DISTANCE_MAX
