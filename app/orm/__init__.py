"""The ORM module.
Contains DB config and the ORM base model.
On import will attempt to connect to local instance of MySQL,
if that fails it will then attempt to connect to production
instance on pythonanywhere.

DB CONFIG
    table name: 'bfriend'
    password: 'letusbfriends'
    user: 'bfriend'
    Commands to get started:
        CREATE USER bfriend@localhost IDENTIFIED BY 'letusbfriends';
        GRANT ALL PRIVILEGES ON *.* TO bfriend@localhost
            WITH GRANT OPTION;
        CREATE DATABASE bfriend;
"""
import peewee as peewee
import logging

logger = logging.getLogger('peewee')
# logger.setLevel(logging.DEBUG)
logger.addHandler(logging.StreamHandler())

connections = [
    peewee.MySQLDatabase(database="bfriend",
                         host="localhost",
                         port=3306,
                         user="bfriend",
                         passwd="letusbfriends",
                         ),
    peewee.MySQLDatabase("bfriend$bfriend",
                         # name="bfriend$bfriend",
                         user=r"bfriend",
                         host=r"bfriend.mysql.pythonanywhere-services.com",
                         port=3306,
                         passwd=r"letusbfriends",
                         )
]

for c in connections:
    try:

        db = c
        # python -c 'import peewee as peewee;import app.users as
        # us;db=peewee.MySQLDatabase("bfriend$bfriend",user="bfriend",host="bfriend.mysql.pythonanywhere-services.com",port=3306,passwd="letusbfriends");db.connect();db.create_table(us.User)'
        db.connect()
        db.close()
    except peewee.OperationalError:
        pass
    else:
        # handle case of successful connection.
        break
else:
    raise RuntimeError(
        "Neither local nor production MySQL instances were available.")


class BaseModel(peewee.Model):
    """The base db object model from which other models inherit.
    """
    class Meta:
        database = db

    def __repr__(self):
        """Represent the Model object as its constructor containing
        all db attributes."""
        return "{}({})".format(
            self.__class__.__name__,
            ', '.join(
                "=".join(map(repr, i)) for i in sorted(
                    self.__dict__["_data"].items(),
                    key=lambda t: t[0])
            )
        )
