import app.users as users
import app.challenges as challenges
import app.questions as questions
import app.db as db
import json


def default_output(raw_path):
    full_path = raw_path.split("/")
    return {"path": raw_path,
            "path_list": full_path,
            "error": None,
            "error_description": ""}


def incoming_handshakes(path):
    full_path = path.split("/")
    out_dict = default_output(path)
    # /api/incoming/username
    if full_path[0] == "incoming":
        try:
            target = users.User.from_username(full_path[1])
            objs = challenges.Objective.incoming_handshakes(target)
            out_dict.update({"target_user": target.name,
                             "num_incoming": len(objs)})
        except (IndexError, users.AuthenticationError):
            out_dict["error"] = True
            out_dict["error_description"] = "Bad target user"
    return json.dumps(out_dict)


def search_universities(name):
    with db.connect() as (_, c):
        results = c.execute("""SELECT name
            FROM universities
            WHERE name LIKE ?
            COLLATE NOCASE
            LIMIT 10""", ("{}%".format(name),)

        ).fetchall()
    matches = []
    out_dict = default_output()
    out_dict.update({"matches": matches})
    return json.dumps(out_dict)
