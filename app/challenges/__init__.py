import app.questions as questions
import app.users as users
import app.config as config
from typing import List
import app.db as db
import app.orm as orm
import app.schools as schools
import peewee as pw
import logging


class ObjectiveNotFoundError(Exception):
    pass


class WeirdoFoundException(Exception):
    pass


class Ranking(orm.BaseModel):
    id = pw.PrimaryKeyField()
    user = pw.ForeignKeyField(users.User, on_delete="CASCADE")
    score = pw.IntegerField()

    class Meta:
        db_table = "rankings"


class Objective(orm.BaseModel):
    """A class to represent an instance of a user
    in a set of two users who answered the same question the same way
    and that first user being assigned the goal of engaging the second.
    """
    id = pw.PrimaryKeyField()
    date = pw.DateTimeField(default=pw.datetime.datetime.now)
    source = pw.ForeignKeyField(
        users.User,
        related_name="source_of",
        on_delete="CASCADE",
        null=False)
    target = pw.ForeignKeyField(
        users.User,
        related_name="target_of",
        on_delete="CASCADE",
        null=True)
    reason = pw.ForeignKeyField(
        questions.Question,
        on_delete="CASCADE",
        null=False)
    points = pw.IntegerField(default=100)
    status = pw.IntegerField(default=0,
                             null=False)

    class Meta():
        db_table = "objectives"

    # Get a user's active objectives
    @classmethod
    def from_id(cls, target_id: str) -> "Objective":
        """Instantiate an Objective by unique id.

        :param target_id: the unique id of the target objective

        """
        return Objective.select().where(Objective.id == target_id).get()

    @classmethod
    def active_objectives(cls, source_user: users.User) -> List["Objective"]:
        """Return a list of a user's active objectives

        :param source_user: the user whose objectives are being requested
        """

        rows = Objective.select().where(
            (Objective.source == source_user.id) &
            (Objective.target != source_user.id) &
            (Objective.status == 0)
        )
        # print([i.source for i in rows])
        valid_rows = []
        for item in rows:
            try:
                # attempt to invoke the users to check validity
                _ = item.source
                _ = item.target
            except:
                item.delete_instance()
            else:
                valid_rows.append(item)
        return list(valid_rows)

    @classmethod
    def incoming_handshakes(cls, target_user: users.User) -> ["Objective"]:
        """Return a list of active objectives that target this user.

        :param target_user: the user whose incoming handshakes are requested

        """
        # retrieve lsit of objectives
        results = Objective.select().where(
            (Objective.source != target_user.id) &
            (Objective.target == target_user.id) &
            (Objective.status == 1)
        )
        return list(results)

    @classmethod
    def completed_handshakes(cls, target_user: users.User) -> ["Objective"]:
        """Return a list of completed objectives that target this user.

        :param target_user: the user whose incoming handshakes are requested

        """
        return list(Objective.select().where(
            (Objective.source != target_user) &
            (Objective.target == target_user) &
            (Objective.status == 2)
        ))

    @classmethod
    def users_points(cls, source_user: users.User) -> int:
        """Return the users points

        :param target_user: the user whose points are being retrieved

        """
        total = sum([o.points for o in Objective.select().where(
            (Objective.source == source_user) &
            (Objective.status == 2)
        )])
        # memoize a user's total points when referred to
        try:
            r = Ranking.select().where((Ranking.user == source_user)).get()
            r.score = total
            r.save()
        except pw.DoesNotExist:
            r = Ranking(user=source_user, score=total)
            r.save()

        return total

        # return Objective.select(pw.fn.Sum(Objective.points)).where(
        #         Objective.source == source_user).scalar()

    @classmethod
    def objectify(cls, source: users.User,
                  target: users.User,
                  reason: questions.Question) -> "Objective":
        """Create a new user in db by username and password.

        :param source: the user who must complete this objective
        :param target: the target the source user must engage
        :param reason: the question that conects the two users
        """
        new_objective, created = Objective.get_or_create(
            source=source, target=target, reason=reason)
        if created:
            new_objective.save()
        return new_objective

    @classmethod
    def new_objective(cls, source_user: users.User) -> "Objective":
        """Create and objective for a target user based on their connections.

        :param source_user: the user for whom to create an objective.

        """
        t1 = questions.Answer.alias()
        t2 = questions.Answer.alias()
        obj_alias = Objective.alias()
        already_exists = Objective.select(
            (obj_alias.source == source_user.id) &
            (obj_alias.target == t2.user) &
            (obj_alias.reason == t1.question)
        )
        q = t1.select(t1.user, t2.user, t1.question).join(t2, on=(
            # answered same question
            (t1.question == t2.question) &
            (t1.user == source_user.id) &
            # the same way
            (t1.response == t2.response) &
            # not the same user
            (t1.user != t2.user) &
            (t2.user != source_user.id) &
            (~(already_exists.exists())))
        ).join(users.User).where(users.User.school == source_user.school)
        try:
            result = q.order_by(pw.fn.rand()).scalar(as_tuple=True)
        except pw.OperationalError:
            result = q.order_by(pw.fn.rand()).scalar(as_tuple=True)

        # create a new objective from the results
        try:
            su = users.User.select().where(
                users.User.id == source_user.id).get()
            tu = users.User.select().where(users.User.id == result[1]).get()
            r = questions.Question.from_id(result[2])
            assert su is not None
            assert tu is not None
            assert r is not None
            # print("Source, target, reason", su, tu, r)
            o, created = Objective.get_or_create(
                source=su,
                target=tu,
                reason=r
            )
            if not created:
                o.save()
            return o
        except TypeError:
            raise WeirdoFoundException("")
        except:
            raise WeirdoFoundException("")

    def erase(self) -> None:
        """Delete tbis objective."""

        self.delete_instance()


def recent_connections(source_user, n=6):
    recent = Objective.select().where(
        (Objective.source == source_user) &
        (Objective.status == 2)
    ).order_by(Objective.date.desc()).limit(n)

    return list(recent)


def local_leaders(local_user, n=10):
    return list(Ranking.select().join(users.User).join(schools.School).where(
        (schools.School.id == local_user.school) &
        (Ranking.score > 0)
    ) .order_by(Ranking.score.desc()).limit(n))


def global_leaders(n=10):
    return list(Ranking.select().where(
        (Ranking.score > 0)).order_by(Ranking.score.desc()).limit(n))
