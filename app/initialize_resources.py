# db lib
import sqlite3
# logging...
import logging
# an OrderedDict is used to map table names to SQL queries because order
# matters
import os
# database connection abstraction layer
import app.db as db
import app.config as config
import app.users as users
import app.challenges as challenges
import app.questions as questions
import app.orm as orm
import app.cdn as cdn
import app.schools as schools
import app.locations as locations
import peewee as pw


def create_paths() -> None:
    """Ensure that the necessary folders exist.
    Create them if they don't exist."""
    for path in config.PATHS:
        if not os.path.exists(path):
            os.makedirs(path)


def create_questions():
    """Inser the default questions into the db if they don't exist yet."""
    with orm.db.atomic():
        for q in config.DEFAULT_QUESTIONS:
            try:
                old = questions.Question.select(
                ).where(questions.Question.body == q[0])
                if not old.exists():
                    new_q = questions.Question(body=q[0], answers=q[1])
                    new_q.save()
            except pw.IntegrityError:
                pass


def universities_list():
    all_schools = [
        "California Baptist University"
    ]
    for s in all_schools:
        try:
            _ = schools.School.by_name(s)
        except pw.DoesNotExist:
            to_insert = schools.School(name=s)
            to_insert.save()


def orm_init():
    tables = [
        schools.School,
        users.User,
        users.LoginEvent,
        cdn.File,
        questions.Question,
        questions.Answer,
        challenges.Objective,
        challenges.Ranking,
        locations.UserLocation,

    ]
    orm.db.create_tables(tables, safe=True)


def main() -> None:
    logging.basicConfig(level=logging.INFO)
    orm.db.connect()
    create_paths()
    orm_init()
    create_questions()
    universities_list()
    orm.db.close()
    # universities_list()


if __name__ == '__main__':
    main()
