#!/usr/bin/env python3
import app.users as users
import faker
import app.cdn as cdn
import requests
import json
import random
NUM_USERS = 100


def main():
    f = faker.Factory.create()
    n = 0
    for i in range(NUM_USERS):
        if i % (NUM_USERS // 10) == 0:
            print(100 * i / NUM_USERS, "%")
        try:
            try:
                person = json.loads(
                    requests.get("https://randomuser.me/api/").text
                )["results"][0]
                name = "{}_{}-{}".format(person["name"]["first"],  person[
                    "name"]["last"], random.randint(1, 9999))
                u = users.User.register(name, "lol")
                u.bio = f.text()
                new_file = cdn.File.from_url(person["picture"]["large"])
                u.photo = new_file
                new_file.owner = u
            except requests.exceptions.ProxyError:
                name = f.name().replace(" ", "_") + str(random.randint(1, 9999))
                u = users.User.register(name, "lol")
                u.bio = f.text()
        except users.AuthenticationError:
            pass
            # print("BAD")
        else:
            n += 1
    print("Created {} users of target {}".format(n, NUM_USERS))


if __name__ == '__main__':
    main()
