from distutils.core import setup

setup(
    name="BFriend Application",
    version="0.1.0",
    author="Lunar Studios",
    author_email="bfriend.lunar@gmail.com",
    url="https://bitbucket.org/TheCDC/bfriend_app",
    install_requires=open("requirements.txt").read().split("\n"),
    include_package_data=True,
    package_data={'': ["database.db"]}
)
