# The BFriend App
![logo](https://bitbucket.org/TheCDC/bfriend_app/raw/master/app/static/img/t_logo.png)

[Click here to experience BFriend](http://bfriend.pythonanywhere.com/)

Contributors:

- [Charlie Davidson](https://bitbucket.org/Charlied5/)
- [Christopher Chen](https://bitbucket.org/TheCDC/)
- [Renden Yoder](https://bitbucket.org/rendenyoder/)

# Description
BFriend is a social scavenger hunt game that encourages users to participate in real life exhanges by connecting them through through thought-provoking survey questions.

# Implementation
The BFriend service is a full stack web application written in Python on top of the Flask microframework. The project structure is also that of a Python package and can be installed and executed as such.

# Setup
Make sure to use a Python virtual environment (if you haven't already) by executing `virtualenv env` and enter it with `sourve /env/bin/activate`.

Virtual environments are great because they can run an installation of python isolated from everything else.
Install dependencies into your virtual environment with `pip install -r requirements.txt`

A virtual environment can also be created with the `create_env.sh` script.

# Documentation
To generate documentation, `cd doc` and `bash generate_docs.sh`. If that second command doesn't work try `make html`. The actual documentation can be browsed starting at `doc/_build/html/index.html`
